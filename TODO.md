# ToDO

## Images

### Base
- [x] Java (with args for version)
- [x] Node (with args for version)
- [x] JBang!

### Dev Container
- [x] NVIM
- [x] Maven
- [x] GIT
- [x] Python
- [x] ZSH
- [x] OhMyZSH
- [x] Quarkus

## CI
- [x] build every night
- [x] push to Docker Hub
- [x] Jobs for every version arg of base image
